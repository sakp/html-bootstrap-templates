docker-compose up -d
git pull
subl .

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi


open $URL;