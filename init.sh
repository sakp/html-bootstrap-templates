if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi


echo 'SETUP GIT FTP CONFIGURATION';
git config git-ftp.user ${APP_FTP_USERNAME}
git config git-ftp.url ${APP_FTP_HOST}
git config git-ftp.password ${APP_FTP_PASSWORD}
git config git-ftp.syncroot html/
git config git-ftp.insecure 1
git config git-ftp.remote-root /


if [ ! -d "database" ]
then
    echo 'CREATE DATABASE DIRECTORY';
    mkdir database
fi


echo 'START DOCKER';
docker-compose up -d

echo 'WAIT 20S FOR UP.';
sleep 20

echo 'Init database & import data'

docker exec ${SQL_CONTAINER_NAME} sh -c 'mysql -h localhost -u ${ENV_MYSQL_USERNAME} --password=${ENV_MYSQL_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${ENV_MYSQL_DATABASE} DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"';
if [ -f db.sql ]
then
    cat ${ENV_MYSQL_DATABASE}.sql | docker exec -i ${SQL_CONTAINER_NAME} /usr/bin/mysql -u ${ENV_MYSQL_USERNAME} --password=${ENV_MYSQL_PASSWORD} ${ENV_MYSQL_DATABASE}
fi