<?php /* helper */
error_reporting( error_reporting() & ~E_NOTICE );

define('BASEURL',         '.'); /* base URL */


define('BASECSSURL',      BASEURL.'/assets/css'); /* base CSS URL */
define('BASEJSURL',       BASEURL.'/assets/javascripts'); /* base JAVASCRIPT URL */
define('BASEIMAGEURL',    BASEURL.'/assets/images'); /* base IMAGES URL */


define('FCPATH', 		  dirname(dirname(__FILE__)));
define('UPLOADURL',       BASEURL.'/uploads'); /* base IMAGES URL */
define('PURGE_CACHE',     true); /* base IMAGES URL */
define('MINIFIELD_JS',    false); /* base IMAGES URL */
define('BASEJSPATH',      FCPATH.'/assets/javascripts'); /* base JAVASCRIPT URL */



function _css($css){echo '<link rel="stylesheet" href="'.BASECSSURL.'/'.$css.'" type="text/css"/>';}
function _js($js){echo '<script src="'.BASEJSURL.'/'.$js.'"></script>';}
function _img($img){return BASEIMAGEURL.'/'.$img;}
function is_ajax(){return isset($_SERVER['HTTP_X_REQUESTED_WITH']) || isset($_GET['ajax']);}
function _header(){require_once(FCPATH.'/inc/_header.php');}
function _footer(){require_once(FCPATH.'/inc/_footer.php');}