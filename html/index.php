<?php require_once('./inc/_helper.php');?>
<?php _header(); ?>

<div class="container py-4">
    <div class="row">
        <div class="col">
            <h3>
                Fancy display heading
                <small class="text-muted">With faded secondary text</small>
            </h3>
            <p class="lead">This is a lead paragraph. It stands out from regular paragraphs.</p>
        </div>
    </div>
</div>

<?php _footer(); ?>