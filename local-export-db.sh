if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

echo "EXPORT warinlab -> ${ENV_MYSQL_DATABASE}.sql";
mysqldump -u root --password=qazwsxedc warinlab > db.sql