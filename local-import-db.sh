if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

echo "IMPORT ${ENV_MYSQL_DATABASE}.sql -> warinlab";
cat ${ENV_MYSQL_DATABASE}.sql | mysql -u root --password=qazwsxedc warinlab