if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

echo "IMPORT ${ENV_MYSQL_DATABASE}.sql -> ${ENV_MYSQL_DATABASE}";
cat ${ENV_MYSQL_DATABASE}.sql | docker exec -i ${CONTAINER_NAME} /usr/bin/mysql -u ${ENV_MYSQL_USERNAME} --password=${ENV_MYSQL_PASSWORD} ${ENV_MYSQL_DATABASE}