if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

echo "EXPORT ${ENV_MYSQL_DATABASE} -> ${ENV_MYSQL_DATABASE}.sql";
docker exec ${CONTAINER_NAME} /usr/bin/mysqldump -u ${ENV_MYSQL_USERNAME} --password=${ENV_MYSQL_PASSWORD} ${ENV_MYSQL_DATABASE} > ${ENV_MYSQL_DATABASE}.sql